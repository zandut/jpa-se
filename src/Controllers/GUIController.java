/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import java.awt.CardLayout;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZANDUT
 */
public class GUIController
{
    public void addComponentGroupLayout(JPanel from, JPanel to)
    {
        GroupLayout panel_Layout = (GroupLayout) from.getLayout();
        panel_Layout.setVerticalGroup(
                panel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panel_Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
        );

        panel_Layout.setHorizontalGroup(
                panel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panel_Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
        );
        
    }
    
    public void addComponentCardLayout(JPanel from, JPanel to, String cardName)
    {
       
        from.add(to, cardName);
        
    }
    public void showCard(JPanel from, String cardname)
    {
        CardLayout card = (CardLayout) from.getLayout();
        card.show(from, cardname);
    }
    
    
    
    public void inisialisasiTable(JTable table, String[] column)
    {
        DefaultTableModel tb = new DefaultTableModel(column, 0)
        {

            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false; //To change body of generated methods, choose Tools | Templates.
            }
            
        };
        table.setModel(tb);
    }
    
    public DefaultTableModel getModel(JTable table)
    {
        return (DefaultTableModel) table.getModel();
    }
}
