/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ZANDUT
 */
@Entity
@Table(name = "mahasiswa")
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Mahasiswa.findAll", query = "SELECT m FROM Mahasiswa m"),
    @NamedQuery(name = "Mahasiswa.findByNim", query = "SELECT m FROM Mahasiswa m WHERE m.nim = :nim"),
    @NamedQuery(name = "Mahasiswa.findByLikeNim", query = "SELECT m FROM Mahasiswa m WHERE m.nim LIKE :nim")
})
public class Mahasiswa implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NIM")
    private String nim;
    @Basic(optional = false)
    @Lob
    @Column(name = "NAMA")
    private String nama;
    @Basic(optional = false)
    @Lob
    @Column(name = "PASSWORD")
    private String password;

    public Mahasiswa()
    {
    }

    public Mahasiswa(String nim)
    {
        this.nim = nim;
    }

    public Mahasiswa(String nim, String nama, String password)
    {
        this.nim = nim;
        this.nama = nama;
        this.password = password;
    }

    public String getNim()
    {
        return nim;
    }

    public void setNim(String nim)
    {
        this.nim = nim;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (nim != null ? nim.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mahasiswa))
        {
            return false;
        }
        Mahasiswa other = (Mahasiswa) object;
        if ((this.nim == null && other.nim != null) || (this.nim != null && !this.nim.equals(other.nim)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Models.Mahasiswa[ nim=" + nim + " ]";
    }
    
}
